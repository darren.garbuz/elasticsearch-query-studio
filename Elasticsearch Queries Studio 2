Elasticsearch Queries Studio 2

What is the data type of the location field in the twitter_geo index? What should it be?
text, should be geo_point

Fix the issue with location by editing tweets_geo.sh to explictlly map location as a geo_point field. Hint: To build the JSON that you’ll need to create the new mapping for twitter_geo, you can copy the reponse from fetching the mapping for twitter and modify it.
curl -XPUT 127.0.0.1:9200/tweets_geo -H 'Content-Type: application/json' -d '
{
    "mappings": {
        "_doc": {
            "properties": {
                "date": { "type": "date" },
                "name": { "type": "text" },
                "tweet": { "type": "text" },
                "league": { "type": "text" },
                "user_id": { "type": "long" },
                "likes": { "type": "long" },
                "location": { "type": "geo_point" }
            }
        }
    }
}'

Find all tweets with between 5 and 10 likes, inclusive of those endpoints.
curl -XGET 127.0.0.1:9200/tweets_geo/_search?pretty=true -H 'Content-Type: application/json' -d '
{
    "query": {
        "bool": {
            "must": { "match_all": {} },
            "filter": {
                "range": {
                    "likes": {
                        "gte": 5,
                        "lte":  10
                     }
                }
            }
        }
    }
}'

Find all tweets by Mary Jones that have at least 2 likes.
curl -XGET 127.0.0.1:9200/tweets_geo/_search?pretty=true -H 'Content-Type: application/json' -d '
{
    "query": {
        "bool": {
            "must": {"match": { "name": "Mary Jones" } },
            "filter": {
                "range": {
                    "likes": {
                        "gte": 2
                     }
                }
            }
        }
    }
}'

Find all tweets that contain the text “Elasticsearch”, including mispellings up to distance of 2 away.
curl -XGET 127.0.0.1:9200/tweets_geo/_search?pretty=true -H 'Content-Type: application/json' -d '
{
    "query": {
        "fuzzy": {
            "tweet": {
                "value": "Elasticsearch",
                "fuzziness": 2
            }
        }
    }
}'

Find all tweets that have a location (Hint: Try the exists query https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-exists-query.html)
curl -XGET 127.0.0.1:9200/tweets_geo/_search?pretty=true -H 'Content-Type: application/json' -d '
{
    "query": {
        "exists": {
            "field": "location"
        }
    }
}'

Find all tweets that do not have a location
curl -XGET 127.0.0.1:9200/tweets_geo/_search?pretty=true -H 'Content-Type: application/json' -d '
{
    "query": {
        "bool": {
            "must_not": {
                "exists": {
                    "field": "location"
                }
            }
        }
    }
}'

Find the average number of likes for tweets that have a location
curl -XGET 127.0.0.1:9200/tweets_geo/_search?pretty=true -H 'Content-Type: application/json' -d '
{
    "size": 0,
    "query": {
        "exists": {
            "field": "location"
        }},
        "aggs": {"avg_likes": { "avg": {"field": "likes"}}}

}'


Find the average number of likes for tweets that do not have a location
curl -XGET 127.0.0.1:9200/tweets_geo/_search?pretty=true -H 'Content-Type: application/json' -d '
{
    "size": 0,
    "query": {
        "bool": {
            "must_not": {
                "exists": {
                    "field": "location"
                }
            }
        }
    },
    "aggs": {"avg_likes": { "avg": {"field": "likes"}}}
}'

Find all tweets with locations within 500km of Boise, ID
curl -XGET 127.0.0.1:9200/tweets_geo/_search?pretty=true -H 'Content-Type: application/json' -d '
{
    "query": {
        "bool": {
            "filter": {
                "geo_distance": {
                    "distance": "500km",
                    "location": {
                        "lat": 43.6150,
                        "lon": -116.2023
                    }
                }
            }
        }
    }
}'
